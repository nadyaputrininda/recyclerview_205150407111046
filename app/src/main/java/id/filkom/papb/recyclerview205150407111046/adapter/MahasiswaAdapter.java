package id.filkom.papb.recyclerview205150407111046.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import id.filkom.papb.recyclerview205150407111046.OnMahasiswaListener;
import id.filkom.papb.recyclerview205150407111046.R;
import id.filkom.papb.recyclerview205150407111046.model.Mahasiswa;
import id.filkom.papb.recyclerview205150407111046.viewholder.MahasiswaViewHolder;

import java.util.ArrayList;

public class MahasiswaAdapter extends RecyclerView.Adapter<MahasiswaViewHolder> {

    private ArrayList<Mahasiswa> _mahasiswaList;
    private Context _context;
    private OnMahasiswaListener onMahasiswaListener;

    public MahasiswaAdapter(ArrayList<Mahasiswa> _mahasiswaList, Context _context, OnMahasiswaListener onMahasiswaListener) {
        this._mahasiswaList = _mahasiswaList;
        this._context = _context;
        this.onMahasiswaListener = onMahasiswaListener;
    }

    @NonNull
    @Override
    public MahasiswaViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row, parent, false);
        MahasiswaViewHolder viewHolder = new MahasiswaViewHolder(v, onMahasiswaListener);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MahasiswaViewHolder holder, int position) {

        holder.get_tvNama().setText(_mahasiswaList.get(position).get_nama());
        holder.get_tvNIM().setText(_mahasiswaList.get(position).get_NIM());
        Glide.with(_context).load(_mahasiswaList.get(position).get_foto()).into(holder.get_foto());

    }

    @Override
    public int getItemCount() {
        return _mahasiswaList.size();
    }
}
