package id.filkom.papb.recyclerview205150407111046;

import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class Activity2 extends AppCompatActivity {

    TextView nama_tv, NIM_tv;
    ImageView foto_iv;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);

        nama_tv = findViewById(R.id.tvNama);
        NIM_tv = findViewById(R.id.tvNIM);
        foto_iv = findViewById(R.id.pic);

        String nama = getIntent().getStringExtra("keyNama");
        String NIM = getIntent().getStringExtra("keyNIM");
        int foto = getIntent().getIntExtra("keyFoto", 0);

        nama_tv.setText(nama);
        NIM_tv.setText("NIM : " + NIM);
        if(foto == 2131165271)
        {
            foto_iv.setImageResource(R.drawable.ninda);
        }
        else
        {
            foto_iv.setImageResource(R.drawable.spidey);
        }

    }
}
