package id.filkom.papb.recyclerview205150407111046.viewholder;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import id.filkom.papb.recyclerview205150407111046.OnMahasiswaListener;
import id.filkom.papb.recyclerview205150407111046.R;

public class MahasiswaViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    TextView _tvNama, _tvNIM;
    ImageView _foto;
    Context _context;
    OnMahasiswaListener onMahasiswaListener;

    public MahasiswaViewHolder(@NonNull View itemView, OnMahasiswaListener onMahasiswaListener) {
        super(itemView);
        _context = itemView.getContext();
        _tvNama = itemView.findViewById(R.id.tvNama);
        _tvNIM = itemView.findViewById(R.id.tvNIM);
        _foto = itemView.findViewById(R.id.pic);
        this.onMahasiswaListener = onMahasiswaListener;

        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        onMahasiswaListener.onMahasiswaClick(getAdapterPosition());
    }

    public TextView get_tvNama() {
        return _tvNama;
    }

    public TextView get_tvNIM() {
        return _tvNIM;
    }

    public ImageView get_foto() {
        return _foto;
    }

    public Context get_context() {
        return _context;
    }
}
